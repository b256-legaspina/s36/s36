// Contains all the URI endpoints for our application

const express = require("express");

// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application

const router = express.Router();
const taskController = require("../Controllers/taskControllers.js");

// [SECTION] Routes
// The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
		// They invoke the controller functions from the controller files
		// All the business logic is done in the controller
router.get("/", (req, res) => {

	taskController.getAllTasks().then(resultFromController => {res.send(resultFromController)});
});

//Activity 36 Code

router.get("/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id).then(resultFromController => {res.send(resultFromController)});
});

router.put("/:id/complete", (req, res) => {
	taskController.updateSpecificTask(req.params.id, req.body).then(resultFromController => {
		res.send(resultFromController)
	});
});

module.exports = router;

