// setup the dependencies

const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./Routes/taskRoutes.js");

// Server setup
const app = express();
const port = 4000;

//mongoDB connection
mongoose.connect("mongodb+srv://admin:admin1234@b256legaspina.ssgp2cu.mongodb.net/B256_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

//Checking of connection (checking of connection only)
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`Were connected to the cloud database`));


// Middlewares

app.use(express.json());
app.use(express.urlencoded({extended: true}))

// parentRoute
app.use("/task", taskRoute)

// Server Listening
app.listen(port, () => console.log (`Server is running at port ${port}`));

