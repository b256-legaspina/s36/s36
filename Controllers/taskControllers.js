// Contains all the functions and business logics of our application
const Task = require("../Models/Task.js");

module.exports.getAllTasks = () => {

	return Task.find({}).then(result => {

		return result;
	})
};

//Activity Code s36

module.exports.getSpecificTask = (paramsId) => {

	return Task.findById(paramsId).then(result => {
		return result;
	})
};

module.exports.updateSpecificTask = (paramsId, requestBody) => {

	return Task.findById(paramsId).then((result, err) => {

		if(err) {

			console.log(error);
			return 'Error Found';

		} else {
			result.status = "complete"

			return result.save().then((updatedSpecificTask, err) => {

				if(err) {

					console.log(err);
					return false;

				}else {
					return updatedSpecificTask;
				}
			})
		}
	})
}